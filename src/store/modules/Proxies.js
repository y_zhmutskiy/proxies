import * as mutationTypes from './mutation-types';
import * as actionTypes from './action-types';
import proxies from './defaultProxies'

const state = {
  list: [],
  country: '',
  isAlive: true,
  types: []
};

const getters = {
  countriesList: state => Array.from( new Set( state.list.map(proxie => proxie.country) ) ),
  filteredList: state => state.list.filter(proxie => {
    const isCountryMatch = proxie.country == state.country || !state.country;
    const isAliveMatch = proxie.alive == state.isAlive;
    const isTypeMatch = state.types.includes(proxie.proxy_type) || !state.types.length;
    return  isCountryMatch &&  isAliveMatch && isTypeMatch;
  }),
};

const mutations = {
  [mutationTypes.SET_COUNTRY](state, country) {
    state.country = country;
  },

  [mutationTypes.SET_ALIVE](state, isAlive) {
    state.isAlive = isAlive;
  },

  [mutationTypes.SET_TYPES](state, types) {
    state.types = types;
  },

  [mutationTypes.SET_PROXIES](state, list) {
    state.list = list;
  }
};

const actions = {
  [actionTypes.GET_PROXIES]({ commit }) {
   commit(mutationTypes.SET_PROXIES, proxies);
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};