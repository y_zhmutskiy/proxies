export const SET_COUNTRY = 'SET_COUNTRY';
export const SET_ALIVE = 'SET_ALIVE';
export const SET_TYPES = 'SET_TYPES';
export const SET_PROXIES = 'SET_PROXIES';